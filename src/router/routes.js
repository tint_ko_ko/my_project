import React, { Fragment } from 'react';

// pages
import HomePage from 'pages/home';
import NotFoundPage from 'pages/not-found';

//components
import AppBar from 'components/appBar';

// route
import { renderRoutes } from 'react-router-config';

//style
import './style.css';

const Root = ({ route }) => (
    <div className="main">
        <AppBar />
        {renderRoutes(route.routes)}
    </div>
);

const routes = [
    {
        component: Root,
        routes: [
            {
                component: HomePage,
                path: '/',
                exact: true,
            },
            {
                component: NotFoundPage
            },
        ]
    }
]

export { routes };