import React from 'react';
import PropTypes from 'prop-types';

//constants
import theme from 'constants/theme';

//style
import './style.css';

const Divider = (props) => {
    return (
        <div className="divider"></div>
    )
}

Divider.defaultProps = {
    width: "2px"
}

Divider.propTypes = {
    width: PropTypes.string
}


export default Divider;