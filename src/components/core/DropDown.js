import React, { useState } from 'react';
import PropTypes from 'prop-types';

//constants
import theme from 'constants/theme';

//style
import './style.css';


const DropDown = (props) => {
    const { options } = props;

    const [value, setValue] = useState(options[0]);

    const onClick = value => {
        setValue(value);
    }

    return (
        <div class="dropdown">
            <button
                class="btn dropdown-toggle"
                type="button"
                id="dropdownMenuButton"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false">{value}</button>
            <div class="dropdown-menu"
                aria-labelledby="dropdownMenuButton">
                {
                    options.map((option, index) => (
                        <a class="dropdown-item" href="#" key={index} onClick={() => onClick(option)}>{option}</a>
                    ))
                }
            </div>
        </div>
    )
}

DropDown.defaultProps = {
    options: []
}

DropDown.propTypes = {
    options: PropTypes.array.isRequired
}


export default DropDown;