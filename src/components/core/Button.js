import React from 'react';
import PropTypes from 'prop-types';

//constants
import theme from 'constants/theme';

//style
import './style.css';

const Button = (props) => {
    return (
        <button
            type="button"
            className="btn"
            aria-label="button"
            {...props}>
            {props.value}
        </button>
    )
}

Button.defaultProps = {
    value: ""
}

Button.propTypes = {

}


export default Button;