import React from 'react';
import PropTypes from 'prop-types';

//constants
import theme from 'constants/theme';

//style
import './style.css';

const Icon = (props) => {
    return (
        <i className={`${props.name} icon`} {...props} />
    )
}

Icon.defaultProps = {
    name: ""
}

Icon.propTypes = {
    name: PropTypes.string.isRequired
}


export default Icon;