import React from 'react';
import PropTypes from 'prop-types';

//constants
import theme from 'constants/theme';

//style
import './style.css';

const Text = (props) => {
    return (
        <span className="text" {...props}>{props.value}</span>
    )
}

Text.defaultProps = {
    value: ""
}

Text.propTypes = {
    value: PropTypes.string.isRequired
}


export default Text;