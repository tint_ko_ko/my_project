import React from 'react';

//components
import Text from 'components/core/Text';
import Button from 'components/core/Button';
import Icon from 'components/core/Icon';

const AppBar = (props) => {
    return (
        <nav className="navbar navbar-expand-lg static-top">
            <div className="container">
                <a className="navbar-brand" href="#">
                    <img src="http://placehold.it/150x50?text=Logo" alt="" />
                </a>
                <Button
                    className="navbar-toggler"
                    data-toggle="collapse"
                    data-target="#nav-bar"
                    aria-controls="nav-bar"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
                    value={<Icon name="fa fa-home" />} />
                <div
                    className="collapse navbar-collapse"
                    id="nav-bar">
                    <ul className="navbar-nav ml-auto">
                        <li className="nav-item active">
                            <a className="nav-link" href="#">
                                <Icon name="fa fa-home" style={{ color: '#3500D3' }} />
                                <Text value="Home" style={{ color: '#3500D3' }} />
                            </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#">
                                <Icon name="fa fa-home" />
                                <Text value="Home" />
                            </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#">
                                <Icon name="fa fa-home" />
                                <Text value="Home" />
                            </a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#">
                                <Icon name="fa fa-home" />
                                <Text value="Home" />
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav >
    )
}

export default AppBar;