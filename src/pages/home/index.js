import React from 'react';

//components
import SearchBox from './components/SearchBox';

const Home = (props) => {
    return (
        <div>
            <SearchBox />
        </div>
    );
}

export default Home;