import React from 'react';
import PropTypes from 'prop-types';

//components
import DropDown from 'components/core/DropDown';
import Divider from 'components/core/Divider';


//style
import '../style.css';

const SearchBox = (props) => {
    return (
        <div className="search-box">
            <div className="search-box-item shadow-lg">
                <input
                    type="text"
                    aria-describedby="emailHelp"
                    placeholder="What are you looking for ?" />
                <Divider />
                <DropDown options={["Yangon", "Mandalay", "Pyin Oo Lwin"]} />
            </div>
        </div>
    )
}

SearchBox.defaultProps = {

}

SearchBox.propTypes = {

}


export default SearchBox;