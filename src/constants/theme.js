const colors = {
    primary: "#3500D3",
    secondary: "#212121"
}

export const theme = {
    colors
}