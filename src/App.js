import React from 'react';

// router
import { routes, history } from 'router';
import { Router } from 'react-router-dom';
import { renderRoutes } from 'react-router-config';


function App() {
  return (
    <Router history={history}>
      {renderRoutes(routes)}
    </Router>
  )
}

export default App;
